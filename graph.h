#ifndef Graph_tree
#define Graph_tree
#include<list>
#include<iostream>

using namespace std;

class Node;


class Edge 
{	
public:
	Edge(Node *org, Node *dest, int dist)
	{
		source = org;
		destination = dest;
		distance = dist;
	}

	Node* getOrigin() { return source; }
	Node* getDestination() { return destination; }
	int getDistance() { return distance; }
	
private:
	Node *source;
	Node *destination;
	int distance;
	
};

class Node
{
public:
	Node(char id)
	{
		name = id;
	}

	void addEdge(Node *v, int dist)
	{
		Edge newEdge(this, v, dist);
		edges.push_back(newEdge); // pushback edges in to the list
	}

	void printEdges()
	{
		cout << name << ":" << endl; //print strat node + :
		for (list<Edge>::iterator it = edges.begin(); it != edges.end(); it++)
		{
			Edge e = *it;
			cout << e.getDestination()->getName() << " - " << e.getDistance() << endl; //print "destination - number of distance"
		}
		cout << endl;
	}

	char getName() { return name; } //get name
	list<Edge> getEdges() { return edges; } //get edge value
	

private:
	char name;
	list<Edge> edges; 
};

class Graph
{
public:
	void insert(int data[4][4]) //to insert 4x4 metrix to a graph
	{
		int i = 0;
		for (int i = 0; i < 4; i++)
		{
			char name = 'A' + i;
			Node v = Node(name);
			vertices.push_back(v); //push back to the vector
		}
		for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
		{
			for (int j = 0; j < 4; j++)
			{
				Node &g = *it;
				char name = 'A' + j;
				Node *temp = new Node(name);
				g.addEdge(temp, data[i][j]); //add edge between data
			}
			i++;
		}
	}

	void printGraph()
	{
		for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
		{
			Node &g = *it;
			g.printEdges(); //print graph "start point + destination + weight"
		}
	}

	bool multigraph() 
	{
		return false; //metrix can't represent multiGraph
	}

	bool CompleteGraph()
	{
		int i = 0;
		//checking all vertice
		for (auto it = vertices.begin(); it != vertices.end(); it++)
		{
			Node vertices = *it;
			list <Edge> listedges = vertices.getEdges(); //get vertice edge
			for (auto it2 = listedges.begin(); it2 != listedges.end(); it2++)
			{
				Node *source = it2->getOrigin(); //get source vertice
				Node *dest = it2->getDestination(); //get destnition vertice
				i++;
				if (source->getName() == dest->getName())
				{
					i--; //minus the edge that psuedo
				}
			}
		}

		if (i == vertices.size()*(vertices.size() - 1))//formula to check complete graph
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool WeightedGraph()
	{
		//check every vertice
		for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
		{
			Node &g = *it;
			list<Edge> e = g.getEdges();//get edge of vertice
			//check each edge of vertice
			for (list<Edge>::iterator it2 = e.begin(); it2 != e.end(); ++it2)
			{
				Edge &Weight = *it2;
				if (Weight.getDistance() != 0) //if the weight not equal to zero
				{
					return true; //weighted graph
				}
			}
		}
		return false;
	}

	bool digraph()
	{
		//loop for fisrt vertice
		for (auto it = vertices.begin(); it != vertices.end(); it++)
		{
			Node vertice1 = *it;
			list <Edge> listedges1 = vertice1.getEdges(); //get first vertice edge

			//loop for each edge of fisrt vertice
			for (auto it2 = listedges1.begin(); it2 != listedges1.end(); it2++)
			{
				Node *source1 = it2->getOrigin(); //get source 
				Node *dest1 = it2->getDestination(); //get destnition 

				//loop for second vertice
				for (auto it3 = vertices.begin(); it3 != vertices.end(); it3++)
				{
					Node vertice2 = *it3;

					//check if it's same vertice or not (same vertice can't be compare)
					if (vertice2.getName() != vertice1.getName())
					{
						list <Edge> listedges2 = vertice2.getEdges(); //get edge of second vertice
						for (auto it4 = listedges2.begin(); it4 != listedges2.end(); it4++)
						{

							Node * source2 = it4->getOrigin(); //get source 
							Node * dest2 = it4->getDestination();//get destnition 

							//if vertice 1 can go to vertice 2 and vertice 2 can go to vertice 1 that mean it isn't directed graph
							if (source1->getName() == dest2->getName() && source2->getName() == dest1->getName() && (source2->getName() != dest1->getName() || source2->getName() != dest2->getName()))
							{
								return false;
							}
						}
					}
				}
			}
		}
		if (WeightedGraph() == false) //if emtpy list 
		{
			return false;
		}
		return true;
	}

	bool pseudograph()
	{
		//check every vertice
			for (auto it = vertices.begin(); it != vertices.end(); it++)
			{
				Node vertice = *it;
				list <Edge> listedges = vertice.getEdges(); //get edge 
				for (auto it2 = listedges.begin(); it2 != listedges.end(); it2++)//check each edge
				{
					Node *source = it2->getOrigin(); //get source 
					Node *dest = it2->getDestination(); //get destnition 

					//if vertice has edge that connect with same source and destination
					//and weigth is not 0
					if (source->getName() == dest->getName() && it2->getDistance() != 0) 
					{
						return true; //it is pseudo graph
					}
				}
			}
		return false;
	}



private:
	list<Node> vertices; 
};


#endif